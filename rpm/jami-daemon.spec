%define name        jami-daemon
%define version     10.0.1.e8dbf2af9
%define release     0

Name:          %{name}
Version:       %{version}
Release:       %{release}%{?dist}
Summary:       Daemon component of Jami
Group:         Applications/Internet
License:       GPLv3+
Vendor:        Savoir-faire Linux
URL:           https://jami.net/
Source:        %{name}-%{version}.tar.gz
Patch0:        revert-ddbe4b088f.diff
Requires:      jami-daemon = %{version}

# Build dependencies
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: cmake
BuildRequires: gcc-c++
BuildRequires: gettext-devel
BuildRequires: glibc-devel
BuildRequires: libtool
BuildRequires: make
BuildRequires: which
#BuildRequires: yasm

BuildRequires: alsa-lib-devel
#BuildRequires: gnutls-devel
#BuildRequires: jsoncpp-devel
#BuildRequires: libXext-devel
#BuildRequires: libXfixes-devel
BuildRequires: libuuid-devel
#BuildRequires: libva-devel
#BuildRequires: libvdpau-devel
BuildRequires: pcre-devel
#BuildRequires: uuid-devel
BuildRequires: yaml-cpp-devel
# SailfishOS:
#BuildRequires: meson
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: asio-devel
BuildRequires: dbus-cxx-tools
BuildRequires: dbus-devel
BuildRequires: ffmpeg-devel >= 4.4
BuildRequires: ffmpeg >= 4.4
BuildRequires: libgcrypt-devel
BuildRequires: gmp-devel
BuildRequires: http-parser-devel >= 2.9.4
BuildRequires: kernel-headers
BuildRequires: libarchive-devel
BuildRequires: libnatpmp-devel
BuildRequires: opus-devel
BuildRequires: pulseaudio-devel
BuildRequires: restinio-devel
BuildRequires: systemd-devel
BuildRequires: systemd-libs
BuildRequires: pkgconfig(dbus-c++-1)
BuildRequires: pkgconfig(dbus-cxx-2.0)
BuildRequires: pkgconfig(expat)
BuildRequires: pkgconfig(fmt) >= 5.3.0
BuildRequires: pkgconfig(gnutls) = 3.6.7
BuildRequires: pkgconfig(jsoncpp)
BuildRequires: pkgconfig(libgit2) >= 1.1.0
BuildRequires: pkgconfig(libpjproject)
BuildRequires: pkgconfig(libsecp256k1)
#                        nettle 3.4 required? contrib has git rev "c180b4d7afbda4049ad265d1366567f62a7a4a3a"
#BuildRequires: pkgconfig(nettle) = 3.4.1
BuildRequires: pkgconfig(nettle) = 3.5.1
BuildRequires: pkgconfig(msgpack) >= 3.2.0
BuildRequires: pkgconfig(libupnp) = 1.8.4
BuildRequires: pkgconfig(opendht) >= 2.2.0
BuildRequires: pkgconfig(openssl) >= 1.1.1
BuildRequires: pkgconfig(speex)
BuildRequires: pkgconfig(speexdsp)
BuildRequires: pkgconfig(webrtc-audio-processing) = 0.3.1


#../upstream/contrib/src/argon2/package.json:    "version": "670229c849b9fe882583688b74eb7dfdc846f9f6",
#../upstream/contrib/src/asio/package.json:    "version": "asio-1-12-2",
#../upstream/contrib/src/ffmpeg/package.json:    "version": "n4.4",
#../upstream/contrib/src/ffnvcodec/package.json:    "version": "5ee2ae591f74f53bd6028344f8690f1558a1f17a",
#../upstream/contrib/src/fmt/package.json:    "version": "5.3.0",
#../upstream/contrib/src/gmp/package.json:    "version": "eb35fdadc072ecae2b262fd6e6709c308cadc07a",
#../upstream/contrib/src/gnutls/package.json:    "version": "3.6.7",
#../upstream/contrib/src/http_parser/package.json:    "version": "2.9.4",
#../upstream/contrib/src/iconv/package.json:    "version": "a4d13b43f8bfc328a9d1d326b0d748d5236613be",
#../upstream/contrib/src/jsoncpp/package.json:    "version": "81065748e315026017c633fca1bfc57cba5b246a",
#../upstream/contrib/src/libarchive/package.json:    "version": "a53d711261f4d5bf2104d9c3616a8602a45ba196",
#../upstream/contrib/src/libgit2/package.json:    "version": "v1.1.0",
#../upstream/contrib/src/media-sdk/package.json:    "version": "intel-mediasdk-19.2.0",
#../upstream/contrib/src/minizip/package.json:    "version": "3.0.0",
#../upstream/contrib/src/msgpack/package.json:    "version": "cpp-3.2.0",
#../upstream/contrib/src/natpmp/package.json:    "version": "6d5c0db6a06036fcd97bdba10b474d5369582dba",
#../upstream/contrib/src/nettle/package.json:    "version": "c180b4d7afbda4049ad265d1366567f62a7a4a3a",
#../upstream/contrib/src/onnx/package.json:    "version": "v1.6.0",
#../upstream/contrib/src/opencv_contrib/package.json:    "version": "4.1.1",
#../upstream/contrib/src/opencv/package.json:    "version": "4.1.1",
#../upstream/contrib/src/opendht/package.json:    "version": "2.2.0",
#../upstream/contrib/src/openssl/package.json:    "version": "OpenSSL_1_1_1-stable",
#../upstream/contrib/src/opus/package.json:    "version": "be8b4fb30945c1ee239471d52e0350c78917ac94",
#../upstream/contrib/src/pjproject/package.json:    "version": "3e7b75cb2e482baee58c1991bd2fa4fb06774e0d",
#../upstream/contrib/src/portaudio/package.json:    "version": "v190600_20161030",
#../upstream/contrib/src/pthreads/package.json:    "version": "pthreads4w-code-v2.10.0-rc",
#../upstream/contrib/src/restinio/package.json:    "version": "a7a10e419d9089c5b8ee63f5e3098c892f22fae4",
#../upstream/contrib/src/secp256k1/package.json:    "version": "0b7024185045a49a1a6a4c5615bf31c94f63d9c4",
#../upstream/contrib/src/speexdsp/package.json:    "version": "SpeexDSP-1.2.0",
#../upstream/contrib/src/upnp/package.json:    "version": "1.8.4",
#../upstream/contrib/src/vpx/package.json:    "version": "c01b6dc8451f09745916c079e036364ab63b93cd",
#../upstream/contrib/src/webrtc-audio-processing/package.json:    "version": "v0.3.1",
#../upstream/contrib/src/x264/package.json:    "version": "5fee86cae91cd7b726db7408a3ed1c4da71fb78c",
#../upstream/contrib/src/yaml-cpp/package.json:    "version": "24fa1b33805c9a91df0f32c46c28e314dd7ad96f",
#../upstream/contrib/src/zlib/package.json:    "version": "8e4e3ead55cdd296130242d86b44b92fde3ea4d4",

%description
This package contains the daemon of Jami, a free software for
universal communication which respects the freedoms and privacy of its
users.

%prep
%setup -q -n %{name}-%{version}/upstream
%patch0 -p1

# remove bundled deps where we have native ones
for d in asio argon2 dbus-cpp ffmpeg fmt gcrypt gmp gnutls http_parser jack jsoncpp libarchive libgit2 http-parser natpmp msgpack nettle opendht openssl opus pjproject pthreads restinio secp256k1 speex speexdsp yaml-cpp zlib
do
	rm -rf %{_builddir}/%{name}-%{version}/upstream/contrib/src/$d
done
echo  "INFO contrib has the following remainig:"
ls -l %{_builddir}/%{name}-%{version}/upstream/contrib/src

%build
# Configure the Jami bundled libraries (ffmpeg & pjproject).
#mkdir -p contrib/native
#pushd contrib/native && \
#    ../bootstrap \
#        --no-checksums \
#        --disable-ogg \
#        --disable-flac \
#        --disable-vorbis \
#        --disable-vorbisenc \
#        --disable-speex \
#        --disable-sndfile \
#        --disable-gsm \
#        --disable-speexdsp \
#        --disable-natpmp && \
#    make list && \
#    make fetch && \
#    make %{_smp_mflags} V=1 && \
#    make %{_smp_mflags} V=1 .ffmpeg
#popd

mkdir -p contrib/native
pushd contrib/native && \
    ../bootstrap \
        --no-checksums \
        --disable-ogg \
        --disable-flac \
        --disable-vorbis \
        --disable-vorbisenc \
        --disable-speex \
        --disable-sndfile \
        --disable-gsm \
        --disable-speexdsp \
        --disable-natpmp && \
    make %{_smp_mflags} V=1 && \
    make %{_smp_mflags} V=1 .ffmpeg
popd

./autogen.sh
#%%configure --disable-static --enable-shared
#%%configure --disable-static --enable-shared \
%configure --enable-shared \
  --enable-accel
#  --disable-plugin \
#  --without-opensl \
#  --without-alsa \
#  --without-jack \
#  --without-portaudio \
#  --with-dbus \
#  --with-speex \
#  --with-speexdsp \
#  --with-webrtcap \
#  --with-natpmp

make %{_smp_mflags} V=1

%install
#%%meson_install
%make_install

%package devel
Summary: Development files of the Jami daemon

%package doc
Summary: Documentation for the Jami daemon

%description devel
This package contains the header files for using the Jami daemon as a library.

%description doc
%{summary}.

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root,-)
%license COPYING
%doc ChangeLog README
%{_libdir}/libring.so.*
%{_prefix}/libexec/jamid
%{_datadir}/jami/ringtones
%{_datadir}/dbus-1/services/*
%{_datadir}/dbus-1/interfaces/*

#%%{_datadir}/dbus-1/interfaces/cx.ring.Ring.*.xml
%{_datadir}/dbus-1/services/cx.ring.Ring.service
#%%{_datadir}/metainfo/net.jami.daemon.metainfo.xml
#%%{_datadir}/ring
###
#%%{_includedir}/dring
###
#%%{_libdir}/jamid


%files devel
%{_includedir}/*
%{_libdir}/libring.so

%files doc
%doc %{_mandir}/man1/jamid*


